/**

@param {Number} limit - number which denotes max scope of prime numbers
@param {Number} p - prime number from 0 to limit
@param {Number} n - number of all shared secrets
@param {Number} t - n - 1
@param {Number} k - arbitrary number to generate coefficients
@param {Number} s - secret number from 0 to k

**/

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
const prompt = (query) => new Promise(resolve => rl.question(query, resolve));

(async () => {
    const limit = Number(await prompt('Podaj liczbe, do ktorej maksymalnie chcesz wyznaczyc liczbe pierwsza: '));
    const p = returnLastPrime(sieveOfEratosthenes(limit));
    const n = Number(await prompt('Podaj liczbe n (na ile podzielic sekret): '));
    const t = n - 1;
    const k = Number(await prompt(`Podaj liczbe k (zakres losowanych liczb dla wspolczynnikow): `));
    const s = Math.floor(Math.random() * k);
    const calculatedShares = splittingSecret(p, n, t, k, s);
    const recoveredSecret = recoveringSecret(p, t, calculatedShares);

    console.log('Secret: ', s);
    console.log('Recovered secret: ', recoveredSecret);

    rl.close();
})()

function splittingSecret(p, n, t, k, s) {
    const randomCoefficients = generateRandomCoefficients(t, k, s);
    const calculatedShares = [];
    let sum;

    for (let i = 1; i <= n; i++) {
        sum = 0;

        for (let j = 0; j < t; j++)
            sum += (randomCoefficients[j] * Math.pow(i, j))

        sum = mod(sum, p);
        calculatedShares.push([i, sum]);
    }
    return calculatedShares;
}

function recoveringSecret(p, t, calculatedShares) {
    const recoveredSecret = [];
    const pointsToRecoverSecret = getRandomShares(t, calculatedShares);
    const x = 0;
    let sum = 0;
    let product;

    for (let i in pointsToRecoverSecret) {
        product = 1;

        for (let j in pointsToRecoverSecret) {
            if (i !== j) {
                product *= ((x - pointsToRecoverSecret[j][0])
                        / (pointsToRecoverSecret[i][0] - pointsToRecoverSecret[j][0]));
            }
        }
        product = mod(product, p);
        sum += product * pointsToRecoverSecret[i][1];
        recoveredSecret.push(product);
    }
    sum = mod(sum, p);
    return sum;
}

function generateRandomCoefficients(t, k, s) {
    let numArr = [];

    numArr[0] = s;
    for (let i = 1; i < t; i++)
        numArr[i] = (Math.floor(Math.random() * k));

    return numArr;
}

function getRandomShares(t, calculatedShares) {
    const randomArr = [];
    const setOfNoRepeatShares = JSON.parse(JSON.stringify(calculatedShares));

    for (let i = 0; i < t; i++) {
        const randomNum = Math.floor(Math.random() * setOfNoRepeatShares.length);
        randomArr.push(setOfNoRepeatShares[randomNum]);
        setOfNoRepeatShares.splice(randomNum, 1);
    }

    return randomArr;
}

function mod(n, m) {
    if (n < 0)
        return (n % m + m) % m;
    else
        return n % m;
}

function sieveOfEratosthenes(limit) {
    const primeNums = [];
    const isPrimeArr = new Array(limit).fill(true);

    for (let i = 2; i <= Math.sqrt(limit); i++) {

        if (isPrimeArr[i]) {
            for (let j = i * i; j < limit; j += i) {
                isPrimeArr[j] = false;
            }
        }
    }

    for (let i = 2; i < limit; i++) {

        if (isPrimeArr[i]) {
            primeNums.push(i);
        }
    }

    return primeNums;
}

function returnLastPrime(primeArr) {
    return primeArr[primeArr.length - 1];
}